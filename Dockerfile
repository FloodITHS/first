FROM ubuntu:latest

RUN apt update

COPY docker-compose.yml .

RUN apt-get install -y git

#RUN adduser git -u 1000

RUN apt-get install -y wget

RUN apt-get install -y gnupg

RUN wget -O gitea https://dl.gitea.io/gitea/1.18.1/gitea-1.18.1-linux-amd64

RUN chmod +x gitea

RUN gpg --keyserver keys.openpgp.org --recv --yes 7C9E68152594688862D62AF62D9AE806EC1592E2
#RUN gpg --verify gitea-1.18.1-linux-amd64.asc gitea-1.18.1-linux-amd64

RUN adduser \
   --system \
   --shell /bin/bash \
   --gecos 'Git Version Control' \
   --group \
   --disabled-password \
   --home /home/git \
   git -u 1000

RUN mkdir -p /var/lib/gitea/{custom,data,log}
RUN chown -R git:git /var/lib/gitea/
RUN chmod -R 774 /var/lib/gitea/
RUN mkdir /etc/gitea
RUN chown git:git /etc/gitea
RUN chmod 774 /etc/gitea
RUN git clone https://gitea.com/FloodITHS/first.git
COPY gitea.service /etc/systemd/system

#RUN systemctl enable gitea

#RUN systemctl start gitea

RUN export GITEA_WORK_DIR=/var/lib/gitea/

RUN cp gitea /usr/local/bin/gitea

USER git

ENV GITEA_WORK_DIR=/var/lib/gitea/
CMD /usr/local/bin/gitea web -c /etc/gitea/app.ini
#CMD ["usr/local/bin/gitea", "web", "--work-path", "var/lib/gitea"] 
