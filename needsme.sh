#!/bin/bash
chmod +x gitea
gpg --keyserver keys.openpgp.org --recv --yes 7C9E68152594688862D62AF62D9AE806EC1592E2
#RUN gpg --verify gitea-1.18.1-linux-amd64.asc gitea-1.18.1-linux-amd64

adduser \
   --system \
   --shell /bin/bash \
   --gecos 'Git Version Control' \
   --group \
   --disabled-password \
   --home /home/git \
   git

mkdir -p /var/lib/gitea/{custom,data,log}
chown -R git:git /var/lib/gitea/
chmod -R 750 /var/lib/gitea/
mkdir /etc/gitea
chown root:git /etc/gitea
chmod 770 /etc/gitea
